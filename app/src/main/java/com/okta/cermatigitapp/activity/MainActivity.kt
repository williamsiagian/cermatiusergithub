package com.okta.cermatigitapp.activity

import android.app.ProgressDialog
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.okta.cermatigitapp.R
import com.okta.cermatigitapp.adapter.RecycleviewAdapterSearchUser
import com.okta.cermatigitapp.model.ResponseSearch
import com.okta.cermatigitapp.service.APIClient
import com.okta.cermatigitapp.service.APISearch
import com.okta.cermatigitapp.widget.EndlessRecyclerOnScrollListener
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    var mRecyclerView: RecyclerView? = null
    var mLayoutManager: LinearLayoutManager? = null
    var mAdapter: RecycleviewAdapterSearchUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mRecyclerView = RVResultSearch
        setStatusBarColor()
        setSearchView()
    }

    private fun setSearchView() {
        SWToolBar?.queryHint = "Username"
        SWToolBar?.onActionViewExpanded()
        SWToolBar?.isIconified = false
        SWToolBar?.clearFocus()
        SWToolBar?.isActivated = true
        SWToolBar?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                consumeAPIusername(query, "1")
                SWToolBar?.clearFocus()
                if (currentFocus != null) {
                    val inputMethodManager =
                        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow(
                        currentFocus?.windowToken,
                        0
                    )
                }
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
        try {
            val mDrawable =
                SearchView::class.java.getDeclaredField("mSearchHintIcon")
            mDrawable.isAccessible = true
            val drawable = mDrawable[SWToolBar] as Drawable
            drawable.alpha = 0
            drawable.setBounds(0, 0, 0, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val searchId = resources.getIdentifier("android:id/search_plate", null, null)
        val v = SWToolBar?.findViewById<View>(searchId)
        v?.setBackgroundResource(android.R.color.transparent)
    }

    private fun consumeAPIusername(query: String, page: String) {
        val pd = ProgressDialog(this, R.style.MyTheme)
        pd.setCancelable(false)
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small)
        pd.show()
        val client = APIClient.createService(APISearch::class.java)
        val call = client.GetUsers(query, page)
        call?.enqueue(object : Callback<ResponseSearch> {
            override fun onResponse(
                call: Call<ResponseSearch>,
                response: Response<ResponseSearch>
            ) {
                if (response.code() == 200) {
                    if (response.body().totalCount!! > 0) {
                        mRecyclerView?.visibility = View.VISIBLE
                        mAdapter = RecycleviewAdapterSearchUser()
                        mAdapter?.setItems(response.body().items)
                        mAdapter?.notifyDataSetChanged()
                        mLayoutManager = LinearLayoutManager(this@MainActivity)
                        mRecyclerView?.layoutManager = mLayoutManager
                        mRecyclerView?.adapter = mAdapter
                        mLayoutManager?.isSmoothScrollbarEnabled = true
                        mRecyclerView?.setHasFixedSize(true)
                        mRecyclerView?.setOnScrollListener(object :
                            EndlessRecyclerOnScrollListener(mLayoutManager!!) {
                            override fun onLoadMore(current_page: Int) {
                                if (response.body().totalCount!! > 30) {
                                    if (mAdapter?.itemCount!! < 1000) {
                                        response.body().items.add(null)
                                        mAdapter?.notifyItemInserted(response.body().items?.size!! - 1)
                                        Handler()
                                            .postDelayed({
                                                response.body().items?.toMutableList()?.removeAt(response.body().items?.size!! - 1)
                                                mAdapter?.notifyItemRemoved(response.body().items!!.size)
                                                if (mAdapter?.itemCount!! < 1000 && mAdapter?.itemCount?.toLong() != response.body().totalCount) {
                                                    val pagebytotal =
                                                        mAdapter?.itemCount
                                                    consumeAPIusernamenextpage(
                                                        query,
                                                        (pagebytotal!! / 30 + 1).toString()
                                                    )
                                                } else {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "Tidak ada Data Lainnya",
                                                        Toast.LENGTH_LONG
                                                    ).show()
                                                }
                                            }, 2000)
                                    }
                                }
                            }
                        })
                    } else {
                        mRecyclerView?.visibility = View.GONE
                        Toast.makeText(
                            applicationContext,
                            "User Tidak Di temukan",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                if (response.code() == 403) {
                    Toast.makeText(
                        applicationContext,
                        "API rate limit exceeded for this IP",
                        Toast.LENGTH_LONG
                    ).show()
                }
                pd.dismiss()
            }

            override fun onFailure(
                call: Call<ResponseSearch>,
                t: Throwable
            ) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                pd.dismiss()
            }
        })
    }

    private fun consumeAPIusernamenextpage(query: String, page: String) {
        val client = APIClient.createService(APISearch::class.java)
        val call = client.GetUsers(query, page)
        call.enqueue(object : Callback<ResponseSearch> {
            override fun onResponse(
                call: Call<ResponseSearch>,
                response: Response<ResponseSearch>
            ) {
                if (response.code() == 200) {
                    if (mAdapter?.itemCount?.toLong() != response.body().totalCount) {
                        for (i in response.body().items!!.indices) {
                            mAdapter?.itemCount?.let {
                                mAdapter?.addItem(
                                    response.body().items?.get(i),
                                    it
                                )
                            }
                        }
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Tidak ada Data lainnya",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    mAdapter?.notifyDataSetChanged()
                }
                if (response.code() == 403) {
                    Toast.makeText(
                        applicationContext,
                        "API rate limit exceeded for this IP",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(
                call: Call<ResponseSearch>,
                t: Throwable
            ) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    fun setStatusBarColor() {
        val window = this.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(
                applicationContext,
                R.color.colorPrimaryDark
            )
        }
    }
}