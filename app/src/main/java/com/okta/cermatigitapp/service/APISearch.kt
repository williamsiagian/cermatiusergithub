package com.okta.cermatigitapp.service

import com.okta.cermatigitapp.model.ResponseSearch
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APISearch {
    @GET("/search/users")
    fun GetUsers(
        @Query("q") username: String?,
        @Query("page") page: String?
    ): Call<ResponseSearch>
}