package com.okta.cermatigitapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.okta.cermatigitapp.R
import com.okta.cermatigitapp.model.Item

class RecycleviewAdapterSearch :
    RecyclerView.Adapter<RecycleviewAdapterSearch.ViewHolder>() {
    private var items: List<Item>? = null
    fun setItems(items: List<Item>?) {
        this.items = items
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_users, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val context = holder.itemView.context
        val imageurl = items!![position].avatarUrl
        val username = items!![position].login
        Glide.with(context)
            .load(imageurl)
            .placeholder(android.R.color.darker_gray)
            .into(holder.imageView)
        holder.textView.text = username
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView
        var textView: TextView

        init {
            imageView = itemView.findViewById(R.id.IVItem)
            textView = itemView.findViewById(R.id.TVDesc)
        }
    }
}