package com.okta.cermatigitapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.okta.cermatigitapp.R
import com.okta.cermatigitapp.model.Item

class RecycleviewAdapterSearchUser : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val VIEW_ITEM = 1
    val VIEW_PROG = 0
    private var items: List<Item?>? = null
    fun setItems(items: List<Item?>?) {
        this.items = items
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val vh: RecyclerView.ViewHolder
        vh = if (viewType == VIEW_ITEM) {
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_users, parent, false)
            TextViewHolder(v)
        } else {
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_loading_item, parent, false)
            ProgressViewHolder(v)
        }
        return vh
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        if (holder is TextViewHolder) {
            val context = holder.itemView.context
            val imageurl = items!![position]!!.avatarUrl
            val username = items!![position]!!.login
            Glide.with(context)
                .load(imageurl)
                .placeholder(android.R.color.darker_gray)
                .into(holder.imageView)
            holder.textView.text = username
        } else {
            (holder as ProgressViewHolder).progressBar.isIndeterminate = true
        }
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    fun addItem(item: Item?, position: Int) {
        items?.toMutableList()?.add(position, item)
        notifyItemChanged(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (items!![position] != null) VIEW_ITEM else VIEW_PROG
    }

    class ProgressViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var progressBar: ProgressBar

        init {
            progressBar = v.findViewById<View>(R.id.progressBar1) as ProgressBar
        }
    }

    inner class TextViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView
        var textView: TextView

        init {
            imageView = itemView.findViewById(R.id.IVItem)
            textView = itemView.findViewById(R.id.TVDesc)
        }
    }
}